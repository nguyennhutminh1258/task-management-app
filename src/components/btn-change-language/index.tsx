import { useEffect, useState } from "react";
import vietnam from "../../assets/flag/vietnam.png";
import usa from "../../assets/flag/united-states.png";
import { Button } from "../ui/button";

export const BtnChangeLanguage = () => {
  const [languageId, setLanguageId] = useState<number>(
    Number(localStorage.getItem("languageId")) ?? 1
  );

  const handleChangeLanguage = () => {
    setLanguageId(languageId == 1 ? 2 : 1);
    window.location.reload();
  };

  useEffect(() => {
    localStorage.setItem("languageId", String(languageId));
  }, [languageId]);

  return (
    <Button variant={"outline"} size="sm">
      {languageId !== 1 ? (
        <div
          onClick={handleChangeLanguage}
          className="flex items-center justify-center w-full gap-2"
        >
          <img src={vietnam} className="size-3" alt="vietnam-flag" />
          <p>Việt Nam</p>
        </div>
      ) : (
        <div
          onClick={handleChangeLanguage}
          className="flex items-center justify-center w-full gap-2"
        >
          <img src={usa} className="size-3" alt="usa-flag" />
          <p>English</p>
        </div>
      )}
    </Button>
  );
};
