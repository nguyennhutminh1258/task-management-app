import { NavigationKey } from "@/constants/navigation";
import LandingPage from "@/pages/landing-page";
import OrganizationIdPage from "@/pages/organization";
import SelectOrganization from "@/pages/select-organization";
import { Route, Routes } from "react-router";

const MyRouter = () => {
  return (
    <Routes>
      <Route
        path={NavigationKey.landing}
        element={<LandingPage></LandingPage>}
      ></Route>
      <Route
        path={NavigationKey.select_organization}
        element={<SelectOrganization></SelectOrganization>}
      ></Route>
      <Route
        path={NavigationKey.organization_id}
        element={<OrganizationIdPage></OrganizationIdPage>}
      ></Route>
    </Routes>
  );
};

export default MyRouter;
