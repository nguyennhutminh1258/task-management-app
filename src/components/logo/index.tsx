import { cn } from "@/lib/utils";
import { t } from "i18next";
import logo from '../../assets/logo.svg'

export const Logo = () => {
  return (
    <a href="/">
      <div className="items-center hidden transition hover:opacity-75 gap-x-2 md:flex">
        <img src={logo} alt="Logo" className="text-cyan-800 size-6" />
        <p className={cn("text-lg text-cyan-800 font-bold")}>
          {t("task_management")}
        </p>
      </div>
    </a>
  );
};
