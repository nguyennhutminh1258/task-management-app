export const NavigationKey = {
    landing: '/',
    select_organization: "/organization",
    organization_id: "/organization/:organizationId"
}