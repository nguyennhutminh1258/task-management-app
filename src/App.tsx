import { BrowserRouter as Router } from "react-router-dom";
import MyRouter from "./components/my-router";
import { Provider } from "react-redux";
import { store } from "./app/store";
import { I18nextProvider } from "react-i18next";
import i18next from "i18next";
import './i18n/config'

function App() {
  return (
    <Provider store={store}>
      <I18nextProvider i18n={i18next}>
        <Router>
          <MyRouter></MyRouter>
        </Router>
      </I18nextProvider>
    </Provider>
  );
}

export default App;
