import { NavigationKey } from "@/constants/navigation";
import { OrganizationList, useAuth } from "@clerk/clerk-react";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const SelectOrganization = () => {
  const { isSignedIn } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    if (isSignedIn !== undefined && !isSignedIn) {
      navigate(NavigationKey.landing);
    }
  }, [isSignedIn, navigate]);

  return (
    <div className="flex items-center justify-center h-full bg-slate-300">
      <OrganizationList
        hidePersonal={true}
        afterSelectOrganizationUrl="/organization/:id"
        afterCreateOrganizationUrl="/organization/:id"
      />
    </div>
  );
};

export default SelectOrganization;
