import { BtnChangeLanguage } from "@/components/btn-change-language";
import { Logo } from "@/components/logo";
import { Button } from "@/components/ui/button";
import {
  SignInButton,
  SignUpButton,
  UserButton,
  useAuth,
} from "@clerk/clerk-react";
import { t } from "i18next";

export const Navbar = () => {
  const { isSignedIn } = useAuth();
  
  return (
    <div className="fixed top-0 flex items-center w-full px-4 bg-gray-100 border-b shadow-sm h-14">
      <div className="flex items-center justify-between w-full mx-auto md:max-w-screen-2xl">
        <Logo />
        <div className="flex items-center justify-between w-full space-x-4 md:w-auto">
          <BtnChangeLanguage />
          {isSignedIn ? (
            <UserButton
              afterSignOutUrl="/"
              appearance={{
                elements: {
                  avatarBox: {
                    height: 30,
                    width: 30,
                  },
                },
              }}
            />
          ) : (
            <>
              <SignInButton mode="modal" afterSignInUrl="/organization">
                <Button size="sm" variant="outline">
                  {t("login")}
                </Button>
              </SignInButton>
              <SignUpButton mode="modal" afterSignUpUrl="/organization">
                <Button size="sm">{t("get_task_app")}</Button>
              </SignUpButton>
            </>
          )}
        </div>
      </div>
    </div>
  );
};
