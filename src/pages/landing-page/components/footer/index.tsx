import { Logo } from "@/components/logo";
import { Button } from "@/components/ui/button";
import { t } from "i18next";

export const Footer = () => {
  return (
    <div className="fixed bottom-0 w-full p-4 bg-gray-100 border-t">
      <div className="flex items-center justify-between w-full mx-auto md:max-w-screen-2xl">
        <Logo />
        <div className="flex items-center justify-between w-full space-x-4 md:block md:w-auto">
          <Button size="sm" variant="ghost">
            {t("privacy_policy")}
          </Button>
          <Button size="sm" variant="ghost">
            {t("terms_of_service")}
          </Button>
        </div>
      </div>
    </div>
  );
};