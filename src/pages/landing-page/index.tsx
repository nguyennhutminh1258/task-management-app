import { cn } from "@/lib/utils";
import { t } from "i18next";
import { Navbar } from "./components/navbar";
import { Footer } from "./components/footer";

const LandingPage = () => {
  return (
    <div className="h-full bg-slate-100">
      <Navbar />
      <main className="flex items-center justify-center w-full h-full bg-slate-300">
        <div className="flex flex-col items-center justify-center">
          <div className="flex flex-col items-center justify-center">
            <h1 className="mb-6 text-3xl text-center md:text-6xl text-cyan-800">
              {t("task_management_help")}
            </h1>
            <div className="p-2 px-4 pb-4 text-3xl text-white rounded-md md:text-6xl bg-gradient-to-r from-cyan-600 to-zinc-600 w-fit">
              {t("work_forward")}
            </div>
            <div
              className={cn(
                "text-sm md:text-xl text-zinc-600 mt-4 max-w-xs md:max-w-2xl text-center mx-auto"
              )}
            >
              {t("text_landing")}
            </div>
          </div>
        </div>
      </main>
      <Footer />
    </div>
  );
};

export default LandingPage;
