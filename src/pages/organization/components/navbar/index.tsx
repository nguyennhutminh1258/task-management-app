import { BtnChangeLanguage } from "@/components/btn-change-language";
import { Logo } from "@/components/logo";
import { Button } from "@/components/ui/button";
import { OrganizationSwitcher, UserButton } from "@clerk/clerk-react";
import { t } from "i18next";
import { Plus } from "lucide-react";

const Navbar = () => {
  return (
    <div className="fixed top-0 z-50 flex items-center w-full px-4 bg-gray-100 border-b shadow-sm h-14">
      <div className="flex items-center gap-x-4">
        <div className="hidden md:flex">
          <Logo />
        </div>
        <Button
          variant="primary"
          className="rounded-md hidden md:block h-auto py-1.5 px-2 text-white font-medium"
        >
          {t("create")}
        </Button>
        <Button
          variant="primary"
          size="sm"
          className="block rounded-md md:hidden"
        >
          <Plus className="w-4 h-4" />
        </Button>
      </div>

      <div className="flex items-center ml-auto gap-x-3">
        <BtnChangeLanguage />
        <OrganizationSwitcher
          hidePersonal
          afterCreateOrganizationUrl="/organization/:id"
          afterLeaveOrganizationUrl="/organization"
          afterSelectOrganizationUrl="/organization/:id"
          appearance={{
            elements: {
              rootBox: {
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              },
            },
          }}
        />
        <UserButton
          afterSignOutUrl="/"
          appearance={{
            elements: {
              avatarBox: {
                height: 30,
                width: 30,
              },
            },
          }}
        />
      </div>
    </div>
  );
};

export default Navbar;
