import { useAuth } from "@clerk/clerk-react";
import Navbar from "./components/navbar";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { NavigationKey } from "@/constants/navigation";
import Sidebar from "./components/sidebar";

const OrganizationIdPage = () => {
  const { isSignedIn } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    if (isSignedIn !== undefined && !isSignedIn) {
      navigate(NavigationKey.landing);
    }
  }, [isSignedIn, navigate]);
  return (
    <div className="h-full">
      <Navbar />

      <main className="max-w-6xl px-4 pt-20 md:pt-24 2xl:max-w-screen-xl">
        <div className="flex gap-x-7">
          <div className="hidden w-64 shrink-0 md:block">
            <Sidebar />
          </div>
          asdasd
        </div>
      </main>
    </div>
  );
};

export default OrganizationIdPage;
