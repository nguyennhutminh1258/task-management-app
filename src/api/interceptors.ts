import axios, {
    AxiosError,
    AxiosInstance,
    AxiosRequestConfig,
    AxiosResponse,
    InternalAxiosRequestConfig,
  } from "axios";
  
  const onRequest = (config: InternalAxiosRequestConfig) => {
    const accessToken = localStorage.getItem("accessTokenAPI");
  
    const { method, url } = config;
  
    // Set Headers Here
  
    // Check Authentication Here
    config.headers.Authorization = `Bearer ${accessToken}`;
    // Set Loading Start Here
  
    // logOnDev(`🚀 [API] ${method?.toUpperCase()} ${url} | Request`);
    console.log(`🚀 [API] ${method?.toUpperCase()} ${url} | Request`);
    return config;
  };
  
  const onResponse = (response: AxiosResponse) => {
    // logger.debug("this is a successful response");
    return response.data;
  };
  
  const onErrorResponse = (error: AxiosError | Error): Promise<AxiosError> => {
    if (axios.isAxiosError(error)) {
      const { message } = error;
      const { method, url } = error.config as AxiosRequestConfig;
      const { statusText, status } = (error.response as AxiosResponse) ?? {};
  
      // logOnDev(
      //     `🚨 [API] ${method?.toUpperCase()} ${url} | Error ${status} ${message}`
      // );

      console.log(`🚨 [API] ${method?.toUpperCase()} ${url} | Error ${status} ${statusText} ${message}`)
  
      switch (status) {
        case 401: {
          // "Login required"
          break;
        }
        case 403: {
          // "Permission denied"
          break;
        }
        case 404: {
          // "Invalid request"
          break;
        }
        case 500: {
          // "Server error"
          break;
        }
        default: {
          // "Unknown error occurred"
          break;
        }
      }
  
      if (status === 401) {
        // Delete Token & Go To Login Page if you required.
        // sessionStorage.removeItem("token");
      }
    } else {
      // logOnDev(`🚨 [API] | Error ${error.message}`);
      console.log(`🚨 [API] | Error ${error.message}`)
    }
  
    return Promise.reject(error);
  };
  
  export const setupInterceptors = (instance: AxiosInstance): AxiosInstance => {
    instance.interceptors.request.use(onRequest, onErrorResponse);
    instance.interceptors.response.use(onResponse, onErrorResponse);
  
    return instance;
  };
  