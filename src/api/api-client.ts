import axios from 'axios';
import queryString from 'query-string';
import {setupInterceptors} from "./interceptors";

type RequestContentType = 'application/x-www-form-urlencoded' | 'application/json';

const axiosClient = () => {
    return (url: string, requestContentType: RequestContentType = 'application/json') => {
        const headers: Record<string, string> = {
            Accept: 'application/json',
            'Content-Type': requestContentType,
            mode: 'no-cors',
            timeZone: 'SE Asia Standard Time',
            langId: 'vi'
        }

        const axiosClient = axios.create({
            baseURL: url,
            paramsSerializer: (params) => queryString.stringify({params}),
            headers: headers,
        });

        return setupInterceptors(axiosClient);
    };
};

const _axiosClient = axiosClient();

export default {
    client: _axiosClient('https://localhost:7000/api'),
};
